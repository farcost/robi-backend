package main

import (
    "crypto/sha1"
    "encoding/base64"
    "log"
    "math/rand"
    "net/http"
    "regexp"
    "time"

    "github.com/gorilla/mux"
    "github.com/streamrail/concurrent-map"
    "github.com/syndtr/goleveldb/leveldb"
)

// ----------------------------------------------------------
// SMTP

func SendEmail(email string, message string) {
    log.Print("Sending email to ", email)
    // TODO
}

// ----------------------------------------------------------
// Databases

var db_pass_ *leveldb.DB // { key = "email", value = "password hash"}
var db_tokens_ *leveldb.DB // { key = "email", value = "refresh token"}

// ----------------------------------------------------------
// Cache

var access_tokens_cache_ cmap.ConcurrentMap

// ----------------------------------------------------------
// Hash calculator

func Hash(value string) string {
    hasher := sha1.New()
    hasher.Write([]byte(value))
    sha := base64.URLEncoding.EncodeToString(hasher.Sum(nil))
    return sha
}

func GenRndHash() string {
    return RandStringRunes(32)
}

// ----------------------------------------------------------
// Check data

func isEmailValid(e string) bool {
    pattern := "^(((([a-zA-Z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])+(\\.([a-zA-Z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])+)*)|((\\x22)((((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(([\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x7f]|\\x21|[\\x23-\\x5b]|[\\x5d-\\x7e]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])|(\\([\\x01-\\x09\\x0b\\x0c\\x0d-\\x7f]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}]))))*(((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(\\x22)))@((([a-zA-Z]|\\d|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])|(([a-zA-Z]|\\d|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])([a-zA-Z]|\\d|-|\\.|_|~|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])*([a-zA-Z]|\\d|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])))\\.)+(([a-zA-Z]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])|(([a-zA-Z]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])([a-zA-Z]|\\d|-|\\.|_|~|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])*([a-zA-Z]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])))\\.?$"
    match, _ := regexp.MatchString(pattern, e)
    return match
}

func isPasswordValid(p string) bool {
    return len(p) >= 6
}

// ----------------------------------------------------------
// Random generator

var letter_runes_ = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandStringRunes(n int) string {
    b := make([]rune, n)
    for i := range b {
        b[i] = letter_runes_[rand.Intn(len(letter_runes_))]
    }
    return string(b)
}

// ----------------------------------------------------------
// Parsing requests

type HeaderParam struct { key string; value *string }

func FetchHeaderParams(r *http.Request, params ...HeaderParam) bool {
    for _, p := range params {
        if headerValue, ok := r.Header[p.key]; ok {
            if (len(headerValue) != 1 || len(headerValue[0]) == 0) {
                return false
            }
            *p.value = headerValue[0]
        } else {
            return false
        }
    }
    return true
}

func FetchHeaderParam1(r *http.Request, key1 string) (val1 string, ok bool) {
    ok = FetchHeaderParams(r, HeaderParam { key1, &val1 })
    return
}

func FetchHeaderParams2(r *http.Request, key1 string, key2 string) (val1 string, val2 string, ok bool) {
    ok = FetchHeaderParams(r, HeaderParam { key1, &val1 }, HeaderParam { key2, &val2 })
    return
}

// ----------------------------------------------------------
// API

type Route struct {
    path string
    method string
    handler func (http.ResponseWriter, *http.Request)
}

var API = []Route {
    {
        "/v1/login",
        "POST",
        func (w http.ResponseWriter, r *http.Request) {
            var email, password string
            var header_ok bool

            if email, password, header_ok = FetchHeaderParams2(r, "X-Email", "X-Password"); !header_ok {
                log.Print("Invalid arguments are requested; email: ", email)
                http.Error(w, "Invalid arguments", 400)
                return
            }

            if (!isEmailValid(email) || !isPasswordValid(password)) {
                log.Print("Invalid email or password are requested; email: ", email)
                http.Error(w, "Invalid arguments", 400)
                return
            }

            email_b := []byte(email)

            // Comparing passwords

            db_password_hash_b, err := db_pass_.Get(email_b, nil)
            if (err != nil) {
                http.Error(w, "Free email", 204)
                return
            }
            db_password_hash := string(db_password_hash_b)
            password_hash := Hash(password)

            if db_password_hash != password_hash {
                http.Error(w, "Invalid email or password", 403)
                return
            }

            // Fetching refresh token
            var db_refresh_token string

            db_refresh_token_b, err := db_tokens_.Get(email_b, nil)
            if (err != nil) {
                // Generation new refresh token
                db_refresh_token = GenRndHash()
                err = db_tokens_.Put(email_b, []byte(db_refresh_token), nil)
                if err != nil {
                    log.Print("Failed to put new user's refresh token: ", err, "; email: ", email)
                    http.Error(w, "Failed to get refresh token", 500)
                    return
                }
            } else {
                db_refresh_token = string(db_refresh_token_b)
            }

            // Fetching access token
            var access_token string

            if token, ok := access_tokens_cache_.Get(email); ok && len(token.(string)) > 0 {
                access_token = token.(string)
            } else {
                access_token = GenRndHash()
                access_tokens_cache_.Set(email, access_token)
            }

            // Setup response header
            w.Header().Set("X-Refresh-Token", db_refresh_token)
            w.Header().Set("X-Access-Token", access_token)
        },
    },


    {
        "/v1/access",
        "POST",
        func (w http.ResponseWriter, r *http.Request) {
            var email, refresh_token string
            var header_ok bool

            if email, refresh_token, header_ok = FetchHeaderParams2(r, "X-Email", "X-Refresh-Token"); !header_ok {
                log.Print("Invalid arguments are requested; email: ", email)
                http.Error(w, "Invalid arguments", 400)
                return
            }

            if (!isEmailValid(email)) {
                log.Print("Invalid is requested; email: ", email)
                http.Error(w, "Invalid arguments", 400)
                return
            }

            email_b := []byte(email)

            // Fetching refresh token
            var db_refresh_token string

            db_refresh_token_b, err := db_tokens_.Get(email_b, nil)
            if (err != nil) {
                http.Error(w, "Invalid auth info", 403)
                return
            }
            db_refresh_token = string(db_refresh_token_b)
            if (db_refresh_token != refresh_token) {
                http.Error(w, "Invalid auth info", 403)
                return
            }

            // Fetching access token
            var access_token string

            if token, ok := access_tokens_cache_.Get(email); ok && len(token.(string)) > 0 {
                access_token = token.(string)
            } else {
                access_token = GenRndHash()
                access_tokens_cache_.Set(email, access_token)
            }

            // Setup response header
            w.Header().Set("X-Access-Token", access_token)
        },
    },


    {
        "/v1/register",
        "POST",
        func (w http.ResponseWriter, r *http.Request) {
            var email, password string
            var header_ok bool

            if email, password, header_ok = FetchHeaderParams2(r, "X-Email", "X-Password"); !header_ok {
                log.Print("Invalid arguments are requested; email: ", email)
                http.Error(w, "Invalid arguments", 400)
                return
            }

            if (!isEmailValid(email) || !isPasswordValid(password)) {
                log.Print("Invalid email or password are requested; email: ", email)
                http.Error(w, "Invalid arguments", 400)
                return
            }

            email_b := []byte(email)

            // Check existing email

            _, err := db_pass_.Get(email_b, nil)
            if (err == nil) {
                log.Print("Email already in use; email: ", email)
                http.Error(w, "Email already in use", 403)
                return
            }

            // Create new password hash
            password_hash := Hash(password)
            err = db_pass_.Put(email_b, []byte(password_hash), nil)
            if (err != nil) {
                log.Print("Failed to put password hash to db: ", err, "; email: ", email)
                http.Error(w, "Failed to register password", 500)
                return
            }

            // Generate refresh token
            refresh_token := GenRndHash()
            err = db_tokens_.Put(email_b, []byte(refresh_token), nil)
            if (err != nil) {
                log.Print("Failed to put refresh token to db: ", err, "; email: ", email)
                http.Error(w, "Failed to register refresh token", 500)
                return
            }

            // Generate access token
            access_token := GenRndHash()
            access_tokens_cache_.Set(email, access_token)


            // Setup response header
            w.Header().Set("X-Refresh-Token", refresh_token)
            w.Header().Set("X-Access-Token", access_token)
        },
    },


    {
        "/v1/touch",
        "POST",
        func (w http.ResponseWriter, r *http.Request) {
            var email, access_token string
            var header_ok bool

            if email, access_token, header_ok = FetchHeaderParams2(r, "X-Email", "X-Access-Token"); !header_ok {
                log.Print("Invalid arguments are requested; email: ", email)
                http.Error(w, "Invalid arguments", 400)
                return
            }

            if (!isEmailValid(email)) {
                log.Print("Invalid email is requested; email: ", email)
                http.Error(w, "Invalid arguments", 400)
                return
            }

            if cache_access_token, ok := access_tokens_cache_.Get(email); !ok || access_token != cache_access_token.(string) {
                http.Error(w, "Invalid auth info", 403)
                return
            }
        },
    },


    {
        "/v1/forgot",
        "POST",
        func (w http.ResponseWriter, r *http.Request) {
            var email string
            var header_ok bool

            if email, header_ok = FetchHeaderParam1(r, "X-Email"); !header_ok {
                log.Print("Invalid arguments are requested; email: ", email)
                http.Error(w, "Invalid arguments", 400)
                return
            }

            if (!isEmailValid(email)) {
                log.Print("Invalid email is requested; email: ", email)
                http.Error(w, "Invalid arguments", 400)
                return
            }

            email_b := []byte(email)
            _, err := db_pass_.Get(email_b, nil)
            if err != nil {
                http.Error(w, "Email is not exist", 403)
                return
            }
        },
    },
}

// ----------------------------------------------------------
// API Decorators

func DecoratorApiLoggingRequest(old_f func (w http.ResponseWriter, r *http.Request)) (func (w http.ResponseWriter, r *http.Request)) {
    return func (w http.ResponseWriter, r *http.Request) {
        log.Print("-------------------------------------------------- \n", r)
        old_f(w, r)
    }
}

// ----------------------------------------------------------

func main() {
    rand.Seed(time.Now().UnixNano())

    // ------------------------------------------------------
    // Tokens cache
    access_tokens_cache_ = cmap.New()

    // ------------------------------------------------------
    // Databases

    type DBInfo struct { 
        path string
        db_pp **leveldb.DB
    }

    db_infos := []DBInfo {
        DBInfo { "./auth.pass.leveldb.db", &db_pass_ },
        DBInfo { "./auth.tokens.leveldb.db", &db_tokens_ },
    }

    for _, db_info := range db_infos {
        log.Print("Openning db ", db_info.path, "...")
        db, err := leveldb.OpenFile(db_info.path, nil)
        if err != nil {
            log.Fatal("Failed to open leveldb database ", db_info.path, ": ", err)
        }
        log.Print("Leveldb is openned ", db_info.path)

        *db_info.db_pp = db
        defer db.Close()
    }

    // ------------------------------------------------------
    // HTTP Routes

    log.Print("Registering routes...")
    router := mux.NewRouter().StrictSlash(true)
    for _, route := range API {
        router.HandleFunc(route.path, DecoratorApiLoggingRequest(route.handler)).Methods(route.method)
        log.Print("Route registered: ", route.method, " ", route.path)
    }

    // ------------------------------------------------------
    // HTTP Server
    http_port := ":8080"
    log.Print("Listenning ", http_port, " ...")
    log.Fatal(http.ListenAndServe(http_port, router))

    // // ------------------------------------------------------
    // // HTTPS Server
    // https_port := ":443"
    // cert := "server.pem"
    // key := "server.key"
    // log.Print("Listenning ", https_port, " ...")
    // log.Fatal(http.ListenAndServeTLS(https_port, cert, key, nil))
}
