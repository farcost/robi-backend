# Сервис авторизации

----------

### Запрос Refresh токена
```
REQUEST POST: https://auth.api.givemeapp.co/v1/login
HEADER:
  X-Email: EMAIL
  X-Password: PASSWORD
```

*Ответы:*

 - `RESPONSE: 200` - Ок
```
RESPONSE HEADER:
  X-Refresh-Token: REFRESH_TOKEN
  X-Access-Token: ACCESS_TOKEN
```
 - `RESPONSE: 204` - Email свободен для регистрации
 - `RESPONSE: 400` - Неверные аргументы
 - `RESPONSE: 403` - Неверные email или пароль
 - `RESPONSE: 500` - Ошибка сервера

*Пример:*
```
$ curl -I -X POST -H "X-Email: e@example.com" -H "X-Password: 12345" http://localhost:8080/v1/register

HTTP/1.1 200 OK
X-Access-Token: eufoDPNmppKWrcvEVwYjyLycGwuNvDTG
X-Refresh-Token: dCfPhBDWbbcuaezNvffIiLLgtOIKksHa
Date: Fri, 10 Jun 2016 20:05:55 GMT
Content-Length: 0
Content-Type: text/plain; charset=utf-8
```

----------

### Запрос Access токена
```
REQUEST POST: https://auth.api.givemeapp.co/v1/access
HEADER:
  X-Email: EMAIL
  X-Refresh-Token: REFRESH_TOKEN
```

*Ответы:*

 - `RESPONSE: 200` - Ок
```
RESPONSE HEADER:
  X-Access-Token: ACCESS_TOKEN
```
 - `RESPONSE: 400` - Неверные аргументы
 - `RESPONSE: 403` - Неверный email или токен
 - `RESPONSE: 500` - Ошибка сервера

*Пример:*
```
$ curl -I -X POST -H "X-Email: e@example.com" -H "X-Refresh-Token: dCfPhBDWbbcuaezNvffIiLLgtOIKksHa" http://localhost:8080/v1/access

HTTP/1.1 200 OK
X-Access-Token: eufoDPNmppKWrcvEVwYjyLycGwuNvDTG
Date: Fri, 10 Jun 2016 20:14:54 GMT
Content-Length: 0
Content-Type: text/plain; charset=utf-8
```

----------

### Проверка пользовательской сессии
```
REQUEST POST: https://auth.api.givemeapp.co/v1/touch
HEADER:
  X-Email: EMAIL
  X-Access-Token: ACCESS_TOKEN
```

*Ответы:*

 - `RESPONSE: 200` - Ок
 - `RESPONSE: 400` - Неверные аргументы
 - `RESPONSE: 403` - Неверные email или пароль
 - `RESPONSE: 500` - Ошибка сервера

*Пример:*
```
$ curl -I -X POST -H "X-Email: e@example.com" -H "X-Access-Token: eufoDPNmppKWrcvEVwYjyLycGwuNvDTG" http://localhost:8080/v1/touch

HTTP/1.1 200 OK
Date: Fri, 10 Jun 2016 20:17:40 GMT
Content-Length: 0
Content-Type: text/plain; charset=utf-8
```

----------

### Регистрация
```
REQUEST POST: https://auth.api.givemeapp.co/v1/register
HEADER:
  X-Email: EMAIL
  X-Password: PASSWORD
```

*Ответы:*

 - `RESPONSE: 200` - Ок
```
RESPONSE HEADER:
  X-Refresh-Token: REFRESH_TOKEN
  X-Access-Token: ACCESS_TOKEN
```
 - `RESPONSE: 400` - Неверные аргументы
 - `RESPONSE: 403` - Email уже используется
 - `RESPONSE: 500` - Ошибка сервера

*Пример:*
```
$ curl -I -X POST -H "X-Email: e@example.com" -H "X-Password: 12345" http://localhost:8080/v1/register
HTTP/1.1 200 OK

X-Access-Token: gsVjvbYGtHNfKiUlBstiALqYVzPHPgQT
X-Refresh-Token: SiHQXnXskfPWZXBnqqhvHUFQtVdIBvRv
Date: Fri, 10 Jun 2016 20:19:42 GMT
Content-Length: 0
Content-Type: text/plain; charset=utf-8
```

----------

### Восстановление
```
REQUEST POST: https://auth.api.givemeapp.co/v1/forgot
HEADER:
  X-Email: EMAIL
```

*Ответы:*

 - `RESPONSE: 200` - Ок
```
RESPONSE HEADER:
  X-Refresh-Token: REFRESH_TOKEN
  X-Access-Token: ACCESS_TOKEN
```
 - `RESPONSE: 400` - Неверные аргументы
 - `RESPONSE: 403` - Несуществующий email
 - `RESPONSE: 500` - Ошибка сервера

*Пример:*
```
$ curl -I -X POST -H "X-Email: e@example.com" http://localhost:8080/v1/forgot
HTTP/1.1 200 OK

Date: Fri, 10 Jun 2016 20:19:42 GMT
Content-Length: 0
Content-Type: text/plain; charset=utf-8
```

----------
