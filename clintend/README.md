# Сервис API для клиентских приложений

Каждый API запрос должен содержать в заголовке информацию для доступа:
 - `X-Email`
 - `X-Access-Token`

----------

### Запрос списка контактов
```
REQUEST GET: https://api.givemeapp.co/v1/getContacts
```

*Ответы:*

 - `RESPONSE: 200` - Ок
```
RESPONSE BODY JSON:
  [
    {
      "name": "John",
      "desc": "Very good bot",
      "avatar": "http://img.com/1.png"
    },
    {
      "name": "Brad",
      "desc": "Not very good bot",
      "avatar": "http://img.com/2.png"
    }
  ]
```
 - `RESPONSE: 403` - Нет доступа
 - `RESPONSE: 500` - Ошибка сервера

*Пример:*
```
$ curl -I -X POST -H "X-Email: e@example.com" -H "X-Access-Token: Ndj2918QienfjQuenaQjdoPend2938qn" http://localhost:8081/v1/getContacts

HTTP/1.1 200 OK
Date: Fri, 10 Jun 2016 20:05:55 GMT
Content-Length: N
Content-Type: text/plain; charset=utf-8
{ ...json... }
```

----------
