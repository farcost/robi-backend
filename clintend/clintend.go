package main

import (
    "encoding/json"
    "log"
    "net/http"

    "github.com/gorilla/mux"
)

const (
    TOUCH_ACCESS_METHOD = "POST"
    TOUCH_ACCESS_URL = "http://127.0.0.1:8080/v1/touch"
)

// ----------------------------------------------------------
// Parsing requests

type HeaderParam struct { key string; value *string }

func FetchHeaderParams(r *http.Request, params ...HeaderParam) bool {
    for _, p := range params {
        if headerValue, ok := r.Header[p.key]; ok {
            if (len(headerValue) != 1 || len(headerValue[0]) == 0) {
                return false
            }
            *p.value = headerValue[0]
        } else {
            return false
        }
    }
    return true
}

func FetchHeaderParam1(r *http.Request, key1 string) (val1 string, ok bool) {
    ok = FetchHeaderParams(r, HeaderParam { key1, &val1 })
    return
}

func FetchHeaderParams2(r *http.Request, key1 string, key2 string) (val1 string, val2 string, ok bool) {
    ok = FetchHeaderParams(r, HeaderParam { key1, &val1 }, HeaderParam { key2, &val2 })
    return
}

// ----------------------------------------------------------
// Checking access

func IsAvailableAccess(r *http.Request) (email string, ok bool) {
    var header_ok bool
    if email, header_ok = FetchHeaderParam1(r, "X-Email"); !header_ok {
        log.Print("Invalid user's email")
        ok = false
        return
    }

    client := &http.Client{}
    auth_req, err := http.NewRequest(TOUCH_ACCESS_METHOD, TOUCH_ACCESS_URL, nil)
    if err != nil {
        log.Print("Failed creating auth request: ", err, " for request ", r)
        ok = false
        return
    }

    auth_req.Header = r.Header
    auth_resp, err := client.Do(auth_req)
    if err != nil {
        log.Print("Failed connection to auth backend: ", err, " for auth reques ", auth_req)
        ok = false
        return
    }

    if (auth_resp.StatusCode != 200) {
        log.Print("Invalid access: ", auth_resp)
        ok = false
        return
    }

    ok = true
    return
}

// ----------------------------------------------------------
// API

type Route struct {
    path string
    method string
    handler func (http.ResponseWriter, *http.Request)
}

var API = []Route {

    {
        "/v1/getContacts",
        "GET",
        func (w http.ResponseWriter, r *http.Request) {
            var email string
            var ok bool

            if email, ok = IsAvailableAccess(r); !ok {
                http.Error(w, "Invalid access", 403)
                return
            }

            log.Print("getContacts ", email)

            m := map[string]map[string]string{
                "John" : {
                    "desc": "Very good bot",
                    "avatar": "https://pp.vk.me/c608525/v608525337/23bc/BRjouCn16yI.jpg",
                    "access": "1",
                },
                "Brad" : {
                    "desc": "Not very good bot",
                    "avatar": "https://pp.vk.me/c621118/v621118488/121a2/X70zC-brh5Y.jpg",
                    "access": "0",
                },
                "Den": {
                    "desc": "Pretty very good bot",
                    "avatar": "https://pp.vk.me/c315928/v315928247/49a7/IIzhB5ep21s.jpg",
                    "access": "0",
                },
            }

            js, err := json.Marshal(m)
            if err != nil {
                http.Error(w, "Failed generation json", 500)
                return
            }

            w.Write(js)
        },
    },

    {
        "/v1/getMessages",
        "GET",
        func (w http.ResponseWriter, r *http.Request) {
            var email string
            var ok bool

            if email, ok = IsAvailableAccess(r); !ok {
                http.Error(w, "Invalid access", 403)
                return
            }

            print(email)

            data := ` 
            {
                "John": 
                [
                    {
                        "date": "15.06.2016 15:30:50",
                        "text": "Hello, Bot",
                        "me": "1"
                    },
                    {
                        "date": "15.06.2016 15:30:51",
                        "text": "Hello, Anton",
                        "me": "0"
                    },
                    {
                        "date": "15.06.2016 15:30:52",
                        "text": "stat",
                        "me": "1"
                    },
                    {
                        "date": "15.06.2016 15:30:53",
                        "text": "ok",
                        "me": "0"
                    }
                ],

                "Den": 
                [
                    {
                        "date": "15.06.2016 15:30:50",
                        "text": "Hello, Bot",
                        "me": "1"
                    },
                    {
                        "date": "15.06.2016 15:30:51",
                        "text": "Hello, Anton",
                        "me": "0"
                    },
                    {
                        "date": "15.06.2016 15:30:52",
                        "text": "stat",
                        "me": "1"
                    },
                    {
                        "date": "15.06.2016 15:30:53",
                        "text": "ok",
                        "me": "0"
                    }
                ]
            }
            `
            
            // js, err := json.Marshal(m)
            // if err != nil {
            //     http.Error(w, "Failed generation json", 500)
            //     return
            // }

            w.Write([]byte(data))
        },
    },
}

// ----------------------------------------------------------
// API Decorators

func DecoratorApiLoggingRequest(old_f func (w http.ResponseWriter, r *http.Request)) (func (w http.ResponseWriter, r *http.Request)) {
    return func (w http.ResponseWriter, r *http.Request) {
        log.Print("-------------------------------------------------- \n", r)
        old_f(w, r)
    }
}

// ----------------------------------------------------------

func main() {
    // ------------------------------------------------------
    // HTTP Routes

    log.Print("Registering routes...")
    router := mux.NewRouter().StrictSlash(true)
    for _, route := range API {
        router.HandleFunc(route.path, DecoratorApiLoggingRequest(route.handler)).Methods(route.method)
        log.Print("Route registered: ", route.method, " ", route.path)
    }

    // ------------------------------------------------------
    // HTTP Server
    http_port := ":8081"
    log.Print("Listenning ", http_port, " ...")
    log.Fatal(http.ListenAndServe(http_port, router))
}
